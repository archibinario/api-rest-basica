const express = require('express');
const app = express();

const PORT = 8080;

const errors = require('./errors');

app.set('trust proxy', 1);

app.get('/', (req, res, pre) => {
    res.json({ message: 'Hi!' });
});

app.use(errors.error404);
app.use(errors.errorResponse);

let server = app.listen(PORT, async() => {
    try {
        console.log(`app listening port ${PORT}`);
    } catch (e) {
        console.error({ msg: 'FAIL_STARTING_APP', err: `${e.name}: ${e.message}` });
        closeApp(e);
    }
});

process
    .on('uncaughtException', err => console.error({ msg: 'UNCAUGHT_EXCEPTION', err: err }))
    .on('unhandledRejection', (err, p) => console.error({ msg: 'UNHANDLED_REJECTION', err: err, promise: p }))
    .on('SIGTERM', closeApp)
    .on('SIGINT', closeApp);

function closeApp(error = '') {
    console.log('CLOSING APP', error);
    console.info('CLOSING_HTTP_SERVER', {});
    server.close(async() => {
        console.info('HTTP_SERVER_CLOSED', {});
        process.exit(0);
    });
}