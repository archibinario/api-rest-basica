const path = require('path');
module.exports = {
    apps: [{
        name: 'jalar api test',
        script: 'index.js',
        instances: 1,
        exec_mode: 'fork',
        autorestart: true,
        watch: false,
        max_memory_restart: '1G'
    }],
};