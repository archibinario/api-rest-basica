function error404(req, res, next) {
    throw new Error('URL_NOT_FOUND');
}

function errorResponse(err, req, res, next) {
    return res.json(err);
}

module.exports = {
    error404,
    errorResponse
};